FROM node
WORKDIR /root

RUN apt-get update && apt-get install freeradius -y

# Add API code.
ADD app /root
RUN npm install

# Declare port and entrypoint.
EXPOSE 1812/udp 1813/udp 1645/udp 1646/udp 5000/tcp

# Entrypoint
ENTRYPOINT ["npm", "start"]