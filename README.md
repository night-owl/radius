### API Managed Radius Endpoint
Simple local user based radius authentication endpoint. This module currently provides basic radius functionality, with the intention of providing guest access via sponsorship.
In the future, ldap and azure authentication backeds will be supported a long with a variety of additional options. Almost every aspect of this application can be customized via an API, more documentation to come.

#### Run in Docker
```
docker run -d \
    -p 1812:1812/udp \      # Radius Authentication port.
    -p 1813:1813/udp \      # Radius Accounting port.
    -p 1645:1645/udp \      # Radius Authentication port.
    -p 1646:1646/udp \      # Radius Accounting port.
    -e ES_HOST="<elasticsearch-url>" \
    -e ES_PORT="<elasticsearch-port>" \
    -e ES_AUTH_REQUIRED="true/false" \
    -e ES_AUTH_USERNAME="<elasticsearch-username>" \    # Optional, if authentication is set to true.
    -e ES_AUTH_PASSWORD="<elasticsearch-password>" \    # Optional, if authentication is set to true.
    -e API_ADMIN_USER="admin" \                         # Admin username for the api endpoint (uses basic auth).
    -e API_ADMIN_PASSWORD="admin" \                     # Admin password for the api endpoint (uses basic auth).
    registry.gitlab.com/night-owl/radius:latest

```
