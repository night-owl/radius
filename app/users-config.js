const fs = require("fs");

const log = require("./log");

const es = require("./elasticsearch");
const radius = require("./radius");
const utils = require("./utils");
const notifier = require("./notify");

const Cryptr = require('cryptr');
const cryptr = new Cryptr(utils.env("SECRET_KEY", "svxp8CaSpWGVD5yRWftmdwwazyzXGvsu7fRb3txQJFn6dU6SDGZwmnJTTvBsByNT"));

const process_renewal = (user, dryRun = false) => {
    if (user.auto_renew) {
        log.write("[cron] Processing account auto-renewal...");
        var start_date = ((new Date()).setSeconds((new Date()).getSeconds() + 1));
        var end_date = null;
        switch (user.renew_schedule) {
            case "day":
                end_date = ((new Date()).setHours((new Date()).getHours() + 24));
                break;
            case "week":
                end_date = ((new Date()).setHours((new Date()).getHours() + (24*7)));
                break;
            case "month":
                end_date = ((new Date()).setHours((new Date()).getHours() + (24*30)));
                break;
            case "quarter":
                end_date = ((new Date()).setHours((new Date()).getHours() + (24*90)));
                break;
            case "year":
                end_date = ((new Date()).setHours((new Date()).getHours() + (24*365)));
                break;
            default:
                end_date = ((new Date()).setHours((new Date()).getHours() + 24));
                break;
        }
        user.start_date = start_date;
        user.end_date = end_date;

        user.password = Math.random().toString(36).slice(-8);

        if (!dryRun) {
            notifier.notify_password({
                username: user.username,
                email: user.email,
                sponsor: user.sponsor,
                password: user.password
            });
            es.user_put_object(user, () => {
                log.write("[cron] Auto-Renewal complete.");
            });
        }else {
            return true;
        }
        
    }else {
        if (!dryRun) {
            es.user_delete(user.username, () => {
                log.write(`[cron] ${user.username} deleted.`);
            });
        }else {
            return false;
        }
    }
}

const build_config = (users, reload = false) => {
    var config = "";
    if (users) {
        users.forEach(user => {
            var start = new Date(user.start_date);
            var end = new Date(user.end_date);
            var now = new Date();
            if (start < now && end > now) {
log.write(`[info] ---------------------------------------------------
[#   ] Adding user: '${user.name}'
[#   ] Start: ${start}
[#   ] End: ${end}
[-   ] ---------------------------------------------------`);
                config += `
"${user.username}" Cleartext-Password := "${cryptr.decrypt(user.password)}"`;
            } else if (end < now) {
                process_renewal(user);
            }
        });
    } else {
        log.write("[info] No users configured.");
    }
    config += `
    `;
    write_config(config, reload);
}

module.exports = {
    start_monitor: () => {
        user_monitor();
    }
}

var active_users = [];
const user_monitor = () => {
    var user_list = [];
    es.user_get("*", (users) => {
        if (users) {
            users.forEach(user => {
                var start = new Date(user.start_date);
                var end = new Date(user.end_date);
                var now = new Date();
                if (start < now && end > now) {
                    user_list.push(user.name);
                }
            });
            var diff = false;
            user_list.forEach(user => {
                if (!active_users.includes(user)) {
                    diff = true;
                }
            });
            active_users.forEach(user => {
                if (!user_list.includes(user)) {
                    diff = true;
                }
            });
            if (diff) {
                if (active_users.length > 0){
                    log.write("[cron] User list changed, rebuilding config.");
                }else {
                    log.write("[info] Building user list.");
                }
                build_config(users, (active_users.length > 0));
            }
            active_users = user_list;
        }
        setTimeout(() => {
            user_monitor();
        }, 5000);
    });
}

const write_config = (config, reload = false) => {
    fs.writeFile("/etc/freeradius/users", config, (err) => {
        if (err) {
            log.write("[err ] Failed to write '/etc/freeradius/users'", "ERR");
        } else {
            log.write("[info] Created '/etc/freeradius/users'");
            radius.reload();
        }
    });
}