const { exec } = require('child_process');
const es = require("./elasticsearch");

// /usr/sbin/freeradius -f -X -d /etc/freeradius
const radius_command = "/usr/sbin/freeradius";
const radius_args = ["-f", "-X", "-d", "/etc/freeradius"];
var radius = "";
const log = require("./log");

const split_output = (output) => {
    try {
        return (new Buffer(output,'utf-8').toString()).spit("\n");
    }catch (err){
        return [output];
    }
}

var lastStart = 0;

Number.prototype.millsToHour = () => {
    return this.valueOf() / 1000 / 60 / 60;
};

const restartWatcher = () => {
    if (lastStart != 0) {
        var hoursSinceStart = (new Date ().valueOf().millsToHour() - lastStart.millsToHour());
        if (hoursSinceStart > 8) {
            log.write("Scheduled restart of freeradius service in process...");
            lastStart = new Date().valueOf();
            exec("pkill -9 freerad", (err, stdout, stderr) => {
                radius = require('child_process').spawn(radius_command, radius_args);
                monitor();
                log.write("Scheduled restart of freeradius complete!");
            });
        }
        setTimeout(() => {
            restartWatcher();
        }, 1000 * 60 * 30);
    }else {
        return;
    }
}

const monitor = () => {
    lastStart = new Date().valueOf();
    restartWatcher();
    radius.unref();
    radius.stdout.setEncoding('utf8');
    radius.stdout.on("data", (data) => {
        var logData = new Buffer(data,'utf-8').toString();
        var lines = logData.split("\n")

        es.log_put(logData, (error, response) => {
            log.write(`${error} ${response}`);
        });
        if(!logData.includes("radius-test")) {
            lines.forEach(line => {
                if (line.length > 1) {
                    log.write(`${line}`);
                }
            });
        }
    });
    radius.stderr.setEncoding('utf8');
    radius.stderr.on("data", (data) => {
        var logData = new Buffer(data,'utf-8').toString();
        var lines = logData.split("\n")
        
        es.log_put(logData, (error, response) => {
            log.write(`${error} ${response}`);
        });

        if(!logData.includes("radius-test")) {
            lines.forEach(line => {
                if (line.length > 1) {
                    log.write(`${line}`);
                }
            });
        }
    });
    radius.on('exit', function(code) {
        log.write("[info] Radius stopped.", "ERR");
        lastStart = 0;
    });
}

module.exports = {
    initialize: (callback = null) => {
        exec("cp -r /etc/freeradius/3.0/* /etc/freeradius/", (err, stdout, stderr) => {
            exec("rm -rf /etc/freeradius/3.0/", (err, stdout, stderr) => {
                callback ? callback() : null;
            });
        });
    },
    start: (callback = null) => {
        radius = require('child_process').spawn(radius_command, radius_args);
        monitor();
    },
    stop: (callback = null) => {
        exec("pkill -9 freerad", (err, stdout, stderr) => {
            if (callback) {
                callback();
            }
        });
    },
    restart: (callback = null) => {
        exec("pkill -9 freerad", (err, stdout, stderr) => {
            radius = require('child_process').spawn(radius_command, radius_args);
            monitor();
            if (callback) {
                callback();
            }
        });
    },
    reload: (callback = null) => {
        exec(`kill -HUP $(ps -aux | grep "freerad" | awk '{print $2}' | head -1)`, (err, stdout, stderr) => {
            log.write("Radius service reloaded.")
        });
    },
    status: (callback) => {
        exec("ps -aux", (err, stdout, stderr) => {
            if(!err){
                if (stdout.includes("freerad")) {
                    callback(true);
                }else{
                    callback(false);
                }
            }else {
                callback(false);
            }            
        });
    }
}