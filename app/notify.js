const es = require("./elasticsearch");
var azure = require("./notification_modules/azure");
const log = require("./log");

var AZURE_ENABLED = null;
es.settings_get("AZURE_ENABLED", (val) => {
    if (val) {
        AZURE_ENABLED = val;
    }
});

module.exports = {
    notify_password: (user) => {
        if (AZURE_ENABLED) {
            azure.notify_password(user, () => {
                log.write("[noti] Azure notification sent.");
            })
        }
    }
}