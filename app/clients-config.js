const fs = require("fs");
const es = require("./elasticsearch");
const radius = require("./radius");
const utils = require('./utils.js');
const log = require("./log");

var PSK = Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8);
es.settings_get("PSK", (value) =>{
    if (value) {
        PSK = value;
    }else{
        es.settings_put("PSK", PSK);
    }
});

const base = `
client localhost {
    ipaddr = 127.0.0.1
    proto = *
    secret = ${PSK}
    require_message_authenticator = no
    nas_type         = other  
    limit {
            max_connections = 50
            lifetime = 0
            idle_timeout = 30
    }
}

# IPv6 Client
client localhost_ipv6 {
    ipv6addr        = ::1
    secret          = ${PSK}
}
`;

const build_config = (clients) => {
    var config = base;
        if (clients) {
            clients.forEach(client => {
                var secret = PSK;
                if (client.psk) {
                    secret = client.psk;
                }
                var netmask = client.ip.includes("/") ? client.ip.split("/")[1] : 32;
                var ipAddress = client.ip.includes("/") ? client.ip.split("/")[0] : client.ip;
                if (!config.includes(ipAddress)) {
                    log.write("[info] Adding client '" + client.name + "'.");
                    
config += `
client ${client.name} {
    ipaddr = ${ipAddress}/${netmask}
    secret = ${secret}
}
`;
                }else {
                    log.write(`[info] Client ${client.name} skipped: duplicate entry.`);
                }
                
            });
        }else{
            log.write("[info] No clients configured.", "ERR");
        }
        write_config(config);
}

var active_clients = [];
const client_monitor = () => {
    var client_list = [];
    es.client_get("*", (clients) => {
        if (clients) {
            clients.forEach(client => {
                client_list.push(client.name);
            });
            var diff = false;
            client_list.forEach(client => {
                if (!active_clients.includes(client)) {
                    diff = true;
                }
            });
            active_clients.forEach(client => {
                if (!client_list.includes(client)) {
                    diff = true;
                }
            });
            if (diff || active_clients.length == 0) {
                if (active_clients.length > 0){
                    log.write("[cron] Client list changed, rebuilding config.");
                }else {
                    log.write("[info] Building client list.");
                }
                build_config(clients);
            }
            active_clients = client_list;
        }
        setTimeout(() => {
            client_monitor();
        }, 5000);
    });
}

module.exports = {
    start_monitor: () => {
        client_monitor();
    }
}
const write_config = (config) => {
    fs.writeFile("/etc/freeradius/clients.conf", config, (err) => {
        if (err) {
            log.write("[err ] Failed to write '/etc/freeradius/clients.conf'", "ERR");
        } else {
            log.write("[info] Created '/etc/freeradius/clients.conf'");
            radius.reload();
        }
    });
}
