module.exports = {
  // Get value from enviroment or return 'false'.
  get: (variableName, defaultValue = false) => {
    if (process.env[variableName]) {
      return process.env[variableName];
    } else {
      return defaultValue;
    }
  },
  // Set an enviroment variable.
  set: (variableName, value) => {
    process.env[variableName] = value;
  },
};
