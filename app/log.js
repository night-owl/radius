const env = require('./env');

const colors = {
  'WARN': '\x1b[93m',
  'INFO': '\x1b[97m',
  'ERR ': '\x1b[91m',
  'END': '\x1b[0m',
};
module.exports = {
  write: (message, category = 'INFO') => {
    category = category.toUpperCase();
    if (category.length < 4) {
      category = `${category} `;
    }
    const now = new Date();
    if (env.get('DEBUG_ENABLED', false) || category === "ERR") {
      console.log(`${colors[category] || colors['INFO']}[${now.toLocaleString()}][RADIUS] ${message}${colors['END']}`);
    }
  },
};
