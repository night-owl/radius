// Fetch base env variables for elasticsearch configuration.
const utils = require('./utils.js')
const uuidv4 = require('uuid/v4');
const log = require("./log");

var host = utils.env("ES_HOST", "http://localhost");
var host_port = utils.env("ES_PORT", "9200");
var require_auth = utils.env("ES_AUTH_REQUIRED", false);

var unescape = require('unescape-js');

const Cryptr = require('cryptr');
const cryptr = new Cryptr(utils.env("SECRET_KEY", "svxp8CaSpWGVD5yRWftmdwwazyzXGvsu7fRb3txQJFn6dU6SDGZwmnJTTvBsByNT"));

var ready = false;

// Parse the ES_AUTH_REQUIRED paramenter to check if yes/true were actually strings.
if (require_auth === "yes" || require_auth === "true" || require_auth === true) {
    require_auth = true;
} else {
    require_auth = false;
}

var protocol = "http";
if (host.includes("://")) {
    protocol = host.split("://")[0];
    host = host.split("://")[1];
}
host = host.replace(/\/$/, "");

// Fetch auth paramenters if ES_AUTH_REQUIRED is set to true.
var auth = "";
if (require_auth) {
    auth = utils.env("ES_AUTH_USERNAME", "elastic") + ":" + utils.env("ES_AUTH_PASSWORD", "elastic") + "@";
}
auth = protocol + "://" + auth;

// Connnect to the elasticsearch node.
const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: encodeURI(auth + host + ":" + host_port) });

const indeces = [
    "night-owl-rad-settings",
    "night-owl-rad-log",
    "night-owl-rad-clients",
    "night-owl-rad-users"
]

const base = (callback = null) => {
    indeces.forEach(index => {
        client.indices.exists({
            index: index,
        },
            (err, resp) => {
                if (!resp.body) {
                    client.indices.create({ index: index });
                }
            })
    });
    if (callback) {
        callback();
    }
}

module.exports = {
    // Night Owl Settings and logging.
    initialize: (callback = null) => {
        base(() =>{
            if (callback) {
                callback();
            }                
        });
    },
    user_get: (username, callback) => {
        if (username === "*") {
            client.search({
                index: "night-owl-rad-users",
                size: 10000,
                body: {
                    "query": {
                        match_all: {}
                    }
                }
            },
                (err, resp) => {
                    try {
                        items = []
                        for (var i = 0; i < resp.body.hits.hits.length; i++) {
                            resp.body.hits.hits[i]._source.doc.name = resp.body.hits.hits[i]._id
                            if (resp.body.hits.hits[i]._source.doc) {
                                items.push(resp.body.hits.hits[i]._source.doc)
                            }                            
                        }
                        callback(items);
                    } catch (err) {
                        log.write("[err ] Error fetching users list: " + err);
                        callback(null);
                    }
                }
            );
        } else {
            client.search({
                index: "night-owl-rad-users",
                body: {
                    "query": {
                        "terms": {
                            "_id": [username]
                        }
                    }
                }
            },
                (err, resp) => {
                    try {
                        callback(cryptr.decrypt(resp.body.hits.hits[0]._source.doc.password))
                    } catch (err) {
                        callback(null)
                    }
                }
            );
        }        
    },
    user_put: (username, password, acc_start, acc_end, sponsor = null, email = email, auto_renew = false, renew_schedule = "never", callback = null) => {
        var enc_var = cryptr.encrypt(password);
        client.index({
            index: "night-owl-rad-users",
            refresh: true,
            id: username,
            type: "_doc",
            body: {
                doc: {
                    username: username,
                    email: email,
                    sponsor: sponsor,
                    password: enc_var,
                    start_date: acc_start,
                    end_date: acc_end,
                    auto_renew: auto_renew,
                    renew_schedule: renew_schedule
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    user_put_object: (user_object, callback = null) => {
        user_object.password = cryptr.encrypt(user_object.password);
        client.index({
            index: "night-owl-rad-users",
            refresh: true,
            id: user_object.username,
            type: "_doc",
            body: {
                doc: user_object
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    user_delete: (username, callback = null) => {
        try {
            client.deleteByQuery({
                index: "night-owl-rad-users",
                body: {
                    "query": {
                        "terms": {
                            "_id": [username]
                        }
                    }
                }
            },
                () => {
                    if(callback) {
                        callback(true);
                    }
                }
            )
        } catch (err) {
            log.write("[err ] " + err);
            if(callback) {
                callback(false);
            }
        }

    },
    settings_get: (var_name, callback) => {
        client.search({
            index: "night-owl-rad-settings",
            body: {
                "query": {
                    "terms": {
                        "_id": [var_name]
                    }
                }
            }
        },
            (err, resp) => {
                try {
                    callback(cryptr.decrypt(resp.body.hits.hits[0]._source.doc.value))
                } catch (err) {
                    callback(null)
                }
            }
        );
    },
    settings_put: (var_name, value, callback = null) => {
        var enc_var = cryptr.encrypt(value);
        client.index({
            index: "night-owl-rad-settings",
            refresh: true,
            id: var_name,
            type: "_doc",
            body: {
                doc: {
                    value: enc_var
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    // Device related information and logs.
    client_get: (device_name, callback) => {
        if (device_name === "*") {
            client.search({
                index: "night-owl-rad-clients",
                size: 10000,
                body: {
                    "query": {
                        match_all: {}
                    }
                }
            },
                (err, resp) => {
                    try {
                        items = []
                        for (var i = 0; i < resp.body.hits.hits.length; i++) {
                            resp.body.hits.hits[i]._source.doc.name = resp.body.hits.hits[i]._id
                            if (resp.body.hits.hits[i]._source.doc) {
                                items.push(resp.body.hits.hits[i]._source.doc)
                            }                            
                        }
                        callback(items);
                    } catch (err) {
                        log.write("[err ] Error fetching clients list: " + err);
                        callback(null);
                    }
                }
            );
        } else {
            client.search({
                index: "night-owl-rad-clients",
                body: {
                    "query": {
                        "terms": {
                            "_id": [device_name]
                        }
                    }
                }
            },
                (err, resp) => {
                    try {
                        resp.body.hits.hits[0]._source.doc.name = resp.body.hits.hits[0]._id;
                        callback(resp.body.hits.hits[0]._source.doc)
                    } catch (err) {
                        callback(null)
                    }
                }
            );
        }

    },
    client_put: (device_name, ip, psk, callback = null) => {
        client.index({
            index: "night-owl-rad-clients",
            refresh: true,
            id: device_name,
            type: "_doc",
            body: {
                doc: {
                    ip: ip,
                    psk: psk
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    client_delete: (device_name) => {
        try {
            client.deleteByQuery({
                index: "night-owl-rad-clients",
                body: {
                    "query": {
                        "terms": {
                            "_id": [device_name]
                        }
                    }
                }
            },
                () => {
                    return true;
                }
            )
        } catch (err) {
            log.write("[err ] " + err);
            return false;
        }

    },
    log_put: (log_data, callback = null) => {
        client.index({
            index: "night-owl-rad-logs",
            refresh: true,
            id: uuidv4(),
            type: "_doc",
            body: {
                "@timestamp": new Date(),
                doc: {
                    log: log_data,
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
}