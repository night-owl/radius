const request = require('request');
const qs = require('qs');


const utils = require("../utils");
const es = require("../elasticsearch");

const Cryptr = require('cryptr');
const cryptr = new Cryptr(utils.env("SECRET_KEY", "svxp8CaSpWGVD5yRWftmdwwazyzXGvsu7fRb3txQJFn6dU6SDGZwmnJTTvBsByNT"));

const RESOURCE = "https://graph.microsoft.com";

var CLIENT_ID = null;
es.settings_get("AZURE_CLIENT-ID", (val) => {
    if (val) {
        CLIENT_ID = val;
    }
});

var CLIENT_SECRET = null;
es.settings_get("AZURE_CLIENT-SECRET", (val) => {
    if (val) {
        CLIENT_SECRET = val;
    }
});

var TENANT = null;
es.settings_get("AZURE_TENANT", (val) => {
    if (val) {
        TENANT = val;
    }
});

var SENDER = null;
es.settings_get("AZURE_SENDER", (val) => {
    if (val) {
        SENDER = val;
    }
});

var EMAIL_SUBJECT = "Radius Password Notification";
es.settings_get("AZURE_EMAIL-SUBJECT", (val) => {
    if (val) {
        EMAIL_SUBJECT = val;
    }
});

var SAVE_EMAIL = false;
es.settings_get("AZURE_SAVE-EMAIL", (val) => {
    if (val) {
        SAVE_EMAIL = val;
    }
});

var EMAIL_CONTENT = "The password for the account ${username} has automatically been set to: '${password}'.";
es.settings_get("AZURE_EMAIL-CONTENT", (val) => {
    if (val) {
        EMAIL_CONTENT = val;
    }
});

const get_token = (callback) => {
    var ENDPOINT = `https://login.microsoftonline.com/${TENANT}/oauth2/token`;
    var PAYLOAD = qs.stringify({
        "grant_type": "client_credentials",
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "resource": RESOURCE
    });
    request.get(ENDPOINT, {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: PAYLOAD,
    }, (err, resp, body) => {
        body = JSON.parse(body);
        callback(body.access_token);
    })
}

const send_email = (username, password, email, callback = null) => {
    get_token((token) => {
        var ENDPOINT = `https://graph.microsoft.com/v1.0/users/${SENDER}/sendMail`;
        var PAYLOAD = JSON.stringify({
            "message": {
                "subject": EMAIL_SUBJECT,
                "body": {
                    "contentType": "Html",
                    "content": EMAIL_CONTENT.replace("${username}", username).replace("${password}", password)
                },
                "toRecipients": [
                    {
                        "emailAddress": {
                            "address": email
                        }
                    }
                ]
            },
            "saveToSentItems": SAVE_EMAIL.toString()
        });
        request.post(ENDPOINT, {
            method: "POST",
            body: PAYLOAD,
            headers: {
                "Authorization": "Bearer " + token, 
                "Content-type": "application/json"
            }
        }, (err, resp, body) => {
            if(err){
                callback(false);
            }else{
                callback(true);
            }
        })
    });
}

module.exports = {
    notify_password: (user, callback = null) => {
        if (user.email) {
            send_email(user.username, user.password, user.email, () => {

            });
        }
        if (user.sponsor) {
            send_email(user.username, user.password, user.sponsor, () => {

            });
        }
    }
}