const express = require('express');
var bodyParser = require("body-parser");
var cors = require('cors');
const app = express();
const log = require("./log");

const clients_config = require("./clients-config");
const inner_tunnel_config = require("./inner-tunnel-config");
const radius_config = require("./radius-config");
const users_config = require("./users-config");

const radius = require("./radius");

const es = require("./elasticsearch");
const utils = require("./utils");

const notifier = require("./notify");

const basic_auth = require('express-basic-auth');

const admin_username = utils.env("API_ADMIN_USER", "admin");
const admin_password = utils.env("API_ADMIN_PASSWORD", "admin");

app.use(cors());
app.get("/radius/ready", (req, res) => {
    res.sendStatus(200);
});

app.use(basic_auth({
    users: {
        [admin_username]: admin_password
    },
    unauthorizedResponse: {
        401: "Access denied. This resource is protected."
    }
}))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/radius/status", (req, res) => {
    radius.status((status => {
        res.send(status);
    }));
});

app.get('/radius/clients', (req, res) => {
    try {
        es.client_get("*", (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post('/radius/clients/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        log.write("[info] Adding client " + parts[parts.length - 1]);
        var psk = req.body.psk || null;
        es.client_put(parts[parts.length - 1], req.body.ip, psk, () => {
            res.send(
                {
                    success: true,
                    msg: "Device '" + parts[parts.length - 1] + "' saved."
                }
            )
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.delete('/radius/clients/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        res.send({
            success: es.client_delete(parts[parts.length - 1])
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/radius/clients/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        es.client_get(parts[parts.length - 1], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }

});

app.get('/radius/settings/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        log.write("[info] Fetching setting: " + parts[parts.length - 1]);
        es.settings_get(parts[parts.length - 1], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post('/radius/settings/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        es.settings_put(parts[parts.length - 1], req.body.value, () => {
            res.send(parts[parts.length - 1] + " SAVED.");
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/radius/users', (req, res) => {
    try {
        es.user_get("*", (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/radius/users/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        log.write("[info] Fetching user: " + parts[parts.length - 1]);
        es.user_get(parts[parts.length - 1], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.delete('/radius/users/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        log.write("[info] Deleting user: " + parts[parts.length - 1]);
        es.user_delete(parts[parts.length - 1], (data) => {
            res.send(parts[parts.length - 1] + " DELETED.");
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post('/radius/users/*', (req, res) => {
    try {

        // Do some additional parsing on the date coming from the API and null it if the data is invalid.
        try {
            req.body.start_date = ((new Date(req.body.start_date)).setSeconds((new Date(req.body.start_date)).getSeconds() + 30));
        }catch (err) {
            req.body.start_date = null;
        }
        try {
            req.body.end_date = ((new Date(req.body.end_date)).setHours((new Date(req.body.end_date)).getHours() + 24));
        }catch (err) {
            req.body.end_date = null;
        }

        // Process incoming API data and set defaults if value is not present.
        var parts = req.path.split('/');
        var password = req.body.password || Math.random().toString(36).slice(-8);
        var start_date = req.body.start_date || ((new Date()).setSeconds((new Date()).getSeconds() + 30));
        var end_date = req.body.end_date || ((new Date()).setHours((new Date()).getHours() + 24));
        var auto_renew = req.body.auto_renew || false;
        var renew_schedule = req.body.renew_schedule || "day";
        var sponsor = req.body.sponsor || null;
        var email = req.body.email || null;

        // Set end date based on shedule.
        switch (renew_schedule) {
            case "day":
                end_date = ((new Date(start_date)).setHours((new Date(start_date)).getHours() + 24));
                break;
            case "week":
                end_date = ((new Date(start_date)).setHours((new Date(start_date)).getHours() + (24*7)));
                break;
            case "month":
                end_date = ((new Date(start_date)).setHours((new Date(start_date)).getHours() + (24*30)));
                break;
            case "quarter":
                end_date = ((new Date(start_date)).setHours((new Date(start_date)).getHours() + (24*90)));
                break;
            case "year":
                end_date = ((new Date(start_date)).setHours((new Date(start_date)).getHours() + (24*365)));
                break;
            default:
                end_date = ((new Date(start_date)).setHours((new Date(start_date)).getHours() + 24));
                break;
        }

        log.write(`[API] New user processed:
            ${JSON.stringify({
                username: parts[parts.length - 1],
                password, start_date, end_date, sponsor, email, auto_renew, renew_schedule
            })}
        `);

        es.user_put(parts[parts.length - 1], password, start_date, end_date, sponsor, email, auto_renew, renew_schedule, () => {
            notifier.notify_password({
                username: parts[parts.length - 1],
                email: email,
                sponsor: sponsor,
                password: password
            });
            res.send(parts[parts.length - 1] + " SAVED.");
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/radius/configure', (req, res) => {
    try {
        es.user_get("*", (users) => {
            users_config.write(users);
            es.client_get("*", (clients) => {
                clients_config.write(clients);
                res.send(true);
            });
        });
        inner_tunnel_config.write();
        radius_config.write();
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

const completeStartup = () => {
    radius.initialize(() => {
        // Start monitor for dynamic files.
        users_config.start_monitor();
        clients_config.start_monitor();

        // Write static config files.
        inner_tunnel_config.write();
        radius_config.write();
        
        // Start the radius service.
        setTimeout(() => {
            radius.start();
        }, 5000);
    });
}

es.initialize(() => {
    log.write("[info] Elastic search initialized.");
    completeStartup();
});

const listen_port = utils.env("WEBSERVER_PORT", 5000);
log.write("[info] Listening on http://localhost:" + listen_port);
app.listen(listen_port);